#!/bin/sh
# Prepares VirtualBox client disk for non-booting installation of Tiny Core Linux

die() {
  echo $*
  exit 1
}
ACTIVE_TCE=/etc/sysconfig/tcedir
CD_TCE=/mnt/sr0/cde
HD_TCE=/mnt/sda1/tce

[ $(id -u) == 0 ] || die "Must run as root"
[ -d $CD_TCE/optional ] || die "Need Tiny Core Linux boot CD"
[ -d $ACTIVE_TCE/optional ] || die "Have you booted Tiny Core Linux?"

fdisk /dev/sda <<EOF
n
p
1


w
EOF
mkfs.ext4 /dev/sda1
mkdir /mnt/sda1
mount /dev/sda1 /mnt/sda1
mkdir $HD_TCE
cp -R $CD_TCE/optional $HD_TCE
cp $ACTIVE_TCE/onboot.lst $HD_TCE

# Also copy any freshly downloaded extensions.
cd $ACTIVE_TCE/optional
for i in *; do
  [ -f $HD_TCE/optional/$i ] || cp $i $HD_TCE/optional
done
chown -R tc $HD_TCE
chmod -R u+w $HD_TCE
