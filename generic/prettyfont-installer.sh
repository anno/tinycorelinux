
trap "rm -rf /tmp/font.zip /tmp/prettyfonts" 0
FONTS=/tmp/prettyfonts/usr/local/share/fonts
mkdir -p $FONTS
wget -O /tmp/fonts.zip 'https://www.google.com/fonts/download?kit=_Adf4-WhHej-sE7xvVehE2gYFTLuiQ0Xrr1ICtn4sgfpf0t9BZUIiDiKzT6nkta6EZPD1TvUcgbxXABVfCcZ6hSzEDM9ASGJfbMODYp61QVqpxZ0YzuxrDyCIHvgBhefQLJSZWLWB2TBfpX3gOVXDS3USBnSvpkopQaUR-2r7iU'
cd $FONTS
unzip /tmp/fonts.zip
cd /tmp
mksquashfs prettyfonts /etc/sysconfig/tcedir/optional/prettyfonts.tcz
tce-load -i prettyfonts
